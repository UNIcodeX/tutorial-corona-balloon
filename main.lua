-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local tapCount = 0

local highScore = 0

local background = display.newImageRect( "background.png", 360, 570 )
background.x = display.contentCenterX
background.y = display.contentCenterY

local tapText = display.newText( tapCount, display.contentCenterX, 60, native.systemFont, 100 )
tapText:setFillColor( 0, 0, 0 )


local platform = display.newImageRect( "platform.png", 300, 50 )
platform.x = display.contentCenterX
platform.y = display.contentHeight-25
platform.name = 'platform'

local textHighScore = display.newText("", platform.x, platform.y, native.systemFont, 30 )
--textHighScore:setFillColor( 0, 0, 0 )


local balloon = display.newImageRect( "balloon.png", 112, 112 )
balloon.x = display.contentCenterX
balloon.y = display.contentCenterY
balloon.alpha = 0.7
balloon.name = 'balloon'

local physics = require( "physics" )
physics.start()

physics.addBody( platform, "static" )
physics.addBody( balloon, "dynamic", { radius=50, bounce=0.3 } )

local function onBalloonCollisionWithPlatform( self, event )
	--[[
    if ( event.phase == "began" ) then
        print( self.name .. ": collision began with " .. event.other.name )
 
    elseif ( event.phase == "ended" ) then
        print( self.name .. ": collision ended with " .. event.other.name )
	end
	]]--
	if (tapCount > highScore) then
		highScore = tapCount
		textHighScore.text = "High Score: "..tostring(highScore)
	end

	tapCount = 0
	tapText.text = tapCount
end

local function pushBalloon()
	balloon:applyLinearImpulse( 0, -0.75, balloon.x, balloon.y )
	tapCount = tapCount + 1
	tapText.text = tapCount
end

balloon:addEventListener( "tap", pushBalloon )
platform.collision = onBalloonCollisionWithPlatform
platform:addEventListener( "collision" )
